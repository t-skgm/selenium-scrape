require 'selenium-webdriver'
require 'date'
require 'json'

Driver = Selenium::WebDriver.for :chrome

shinjuku_opac_base_url = 'https://www.library.shinjuku.tokyo.jp/opac/cgi-bin'
new_arrival_path = '/selnew?0&1'

Driver.navigate.to(shinjuku_opac_base_url + new_arrival_path)

new_arrival_clsfctns = Driver.find_element(:class, 'a_block').find_elements(:tag_name, 'a')

new_arrival_ary = new_arrival_clsfctns.map.with_index do |n, idx|
  cat, num = n.text.gsub(/\R|件）$/, '').split(/（| \(/)
  id = n.attribute('href')
  id = $1 if id =~ /opacfile=(\w+)/
  {
    id: id,
    cat: cat,
    num: num.to_i,
    url: n.attribute('href')
  }
end

new_arrival_ary = [new_arrival_ary[0]] # debug

new_arrival_ary.map! do |n|
  u = n[:url].gsub(/count=\d+/, "count=#{n[:num]}")
  Driver.navigate.to u

  trs = Driver.find_element(:class, 'list').find_elements(:tag_name, 'tr')
  trs.map! do |tr|
    td = tr.find_elements(:tag_name, 'td')
    next if td.empty?
    url = td[1].find_element(:tag_name, 'a')&.attribute('href')
    {
      no:     td[0]&.text,
      title:  td[1]&.text,
      url:    url,
      author: td[2]&.text,
      pub:    td[3]&.text,
      status: td[4]&.text,
      kind:   td[5]&.text
    }
  end
  puts "「#{n[:cat]}」をスクレイプ"
  n.merge({books: trs})
end

now_str = DateTime.now.strftime('%Y%m%d_%H%M%S')

File.open("result/#{now_str}.txt", 'w+') do |file|
  new_arrival_ary.each do |h|
    file.puts(h.to_json)
    debugger
  end
end

Driver.quit