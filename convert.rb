require 'json'

all_files = Dir.glob('result/*.txt')
latest_file = all_files.sort.reverse[0]

file = File.open("#{latest_file}", 'r')
lines = file.readlines.map(&:chomp)

def print_result(line)
  text = <<"EOT"
カテゴリー: #{line['cat']}
新着冊数: (#{line['num']}件)

タイトル:
#{print_all_title(line)}

EOT
  puts text
end

def print_all_title(line)
  titles = line['books'].compact.map do |book|
    book['title']
  end
  titles.join("\n")
end

lines.each { |line| print_result(JSON.parse(line)) }
